use std::{
    sync::Mutex,
    fs::File,
    io::BufReader,
    collections::VecDeque,
};
use crate::{Sample, Source};
/// Decodes an Ogg Vorbis stream from a file
pub struct FlacDecoder {
    reader: Mutex<claxon::FlacReader<BufReader<File>>>,
    filename: String,
    buf: VecDeque<Sample>,
}

impl FlacDecoder {
    pub fn new(filename: &str) -> Result<Self, String> {
        let file = BufReader::new(File::open(filename).expect("Error opening file"));
        let stream = match claxon::FlacReader::new(file) {
            Ok(v) => v,
            Err(_) => {
                return Err("Non-flac stream".to_owned());
            },
        };
        Ok(Self {
            reader: Mutex::new(stream),
            filename: filename.to_owned(),
            buf: VecDeque::new(),
        })
    }
}
impl Source for FlacDecoder {
    fn total_length(&self) -> u32 {
        let file = File::open(&self.filename).expect("Error opening file");
        let stream = claxon::FlacReader::new(file).unwrap();
        (stream.streaminfo().samples.unwrap() / stream.streaminfo().sample_rate as u64) as u32
    }
    fn next_sample(&mut self) -> Sample {
        let mut reader = self.reader.lock().unwrap();
        let mut frame_reader = reader.blocks();
        if self.buf.len() < 10 {
            let mut block = claxon::Block::empty();
            let buffer = vec![];
            let mut channel_pos = 0u8;
            let mut sample_pos = 0u32;
            match frame_reader.read_next_or_eof(buffer) {
                Ok(Some(next_block)) => block = next_block,
                Ok(None) => {},
                Err(v) => panic!(v),
            }
            while sample_pos < (block.len() / block.channels()) - 1{
                    self.buf.push_back(Sample::from((block.sample(channel_pos as u32, sample_pos))));
                    if channel_pos == block.channels() as u8 - 1 {
                        channel_pos = 0;
                        sample_pos += 1;
                    } else {
                        channel_pos += 1;
                    }
                }
            }
        if let Some(v) = self.buf.pop_front() {
            Sample::from(v)
        } else {
            Sample::end()
        }
    }
    fn sample_rate(&self) -> u32 {
        let reader = self.reader.lock().unwrap();
        reader.streaminfo().sample_rate
    }
    fn channels (&self) -> u8 {
        let reader = self.reader.lock().unwrap();
        reader.streaminfo().channels as u8
    }
    fn goto (&self, pos: u64) {
    }
}
