//! Decoders of various formats
pub mod vorbis;
pub mod mp3;
pub mod flac;
pub use flac::FlacDecoder;
pub use mp3::Mp3Decoder;
pub use vorbis::VorbisDecoder;
use crate::{Source, Sample};

/// This is a type that iterates over all implemented decoders and automatically chooses the right
/// one at runtime.
pub enum Decoder {
    Vorbis(VorbisDecoder),
    Mp3(Mp3Decoder),
    Flac(FlacDecoder),
}
impl Decoder {
    #![allow(unused_must_use)]
    pub fn new(filename: &str) -> Result<Self, String> {
        match VorbisDecoder::new(filename) {
            Ok(v) => return Ok(Decoder::Vorbis(v)),
            Err(v) => Err::<(), String>(v),
        };
        match Mp3Decoder::new(filename) {
            Ok(v) => return Ok(Decoder::Mp3(v)),
            Err(v) => Err::<(), String>(v),
        };
        match FlacDecoder::new(filename) {
            Ok(v) => return Ok(Decoder::Flac(v)),
            Err(v) => Err::<(), String>(v),
        };
        Err("Unknown format".to_owned())
    }
}
impl Source for Decoder {
    fn next_sample(&mut self) -> Sample {
        match self {
            Self::Vorbis(v) => v.next_sample(),
            Self::Mp3(v) => v.next_sample(),
            Self::Flac(v) => v.next_sample(),
        }
    }
    fn total_length(&self) -> u32 {
        match self {
            Self::Vorbis(v) => v.total_length(),
            Self::Mp3(v) => v.total_length(),
            Self::Flac(v) => v.total_length(),
        }
    }
    fn sample_rate(&self) -> u32 {
        match self {
            Self::Vorbis(v) => v.sample_rate(),
            Self::Mp3(v) => v.sample_rate(),
            Self::Flac(v) => v.sample_rate(),
        }
    }
    fn channels(&self) -> u8 {
        match self {
            Self::Vorbis(v) => v.channels(),
            Self::Mp3(v) => v.channels(),
            Self::Flac(v) => v.channels(),
        }
    }
    fn goto(&self, pos: u64) {
        match self {
            Self::Vorbis(v) => v.goto(pos),
            Self::Mp3(v) => v.goto(pos),
            Self::Flac(v) => v.goto(pos),
        }
    }
}
