use std::{
    sync::Mutex,
    collections::VecDeque,
    io::{BufReader, prelude::*, SeekFrom},
    fs::File,
};
use crate::{Sample, Source};
/// A Source that decodes Mp3 files
pub struct Mp3Decoder {
    reader: Mutex<minimp3::Decoder<BufReader<File>>>,
    filename: String,
    buf: VecDeque<Sample>,
    channels: u8,
    sample_rate: u32,
}

impl Mp3Decoder {
    /// Creates a new "Mp3Decoder" for use in a Sink
    pub fn new (filename: &str) -> Result<Self, String> {
        let file = BufReader::new(File::open(filename).expect("Error opening file"));
        let mut reader = minimp3::Decoder::new(file);
        let buf = VecDeque::new();
        let (sample_rate, channels) = match reader.next_frame() {
            Ok(v) => {
                (v.sample_rate as u32, v.channels as u8)
            }
            Err(_) => return Err("Not valid MP3 data".to_owned()),
        };
        Ok(Self {
            reader: Mutex::new(reader),
            filename: filename.to_owned(),
            buf,
            channels,
            sample_rate,
        })
    }
}
impl Source for Mp3Decoder {
    fn total_length(&self) -> u32 {
        let file = File::open(&self.filename).expect("Error opening file");
        let mut reader = minimp3::Decoder::new(file);
        let mut length = 0;
        loop {
            match reader.next_frame() {
                Ok(v) => length += v.data.len(),
                Err(_) => break,
            }
        }
        length as u32 / self.sample_rate / self.channels as u32
    }
    fn next_sample(&mut self) -> Sample {
        let mut reader = self.reader.lock().unwrap();
        if self.buf.len() <= 10 {
            match reader.next_frame() {
                Ok(v) => {
                    for sample in v.data {
                        self.buf.push_back(Sample::from(sample));
                    }
                },
                _ => {},
            }
        }
        if let Some(v) = self.buf.pop_front() {
            Sample::from(v)
        } else {
            Sample::end()
        }
    }
    fn sample_rate(&self) -> u32 {
        self.sample_rate
    }
    fn channels(&self) -> u8 {
        self.channels
    }
    fn goto(&self, _: u64) {
    }
}
