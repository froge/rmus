use std::{
    sync::Mutex,
    fs::File,
    io::BufReader,
    collections::VecDeque,
};
use crate::{Sample, Source};
/// Decodes an Ogg Vorbis stream from a file
pub struct VorbisDecoder {
    reader: Mutex<lewton::inside_ogg::OggStreamReader<BufReader<File>>>,
    filename: String,
    buf: VecDeque<Sample>,
    pos: u8,
}

impl VorbisDecoder {
    pub fn new(filename: &str) -> Result<Self, String> {
        let file = BufReader::new(File::open(filename).expect("Error opening file"));
        let oggstream = match lewton::inside_ogg::OggStreamReader::new(file) {
            Ok(v) => v,
            Err(_) => {
                return Err("Non-vorbis stream".to_owned());
            },
        };
        Ok(Self {
            reader: Mutex::new(oggstream),
            filename: filename.to_owned(),
            buf: VecDeque::new(),
            pos: 0,
        })
    }
}
impl Source for VorbisDecoder {
    fn total_length(&self) -> u32 {
        let file = File::open(&self.filename).expect("Error opening file");
        let mut reader = ogg::PacketReader::new(file);
        let mut length: u64 = 0;
        let oggstream = self.reader.lock().unwrap();
        loop {
            match reader.read_packet().unwrap() {
                Some(v) => if let Ok(v) = lewton::audio::get_decoded_sample_count(&oggstream.ident_hdr, &oggstream.setup_hdr, &v.data) {
                    length += v as u64;
                }
                None => break,
            }
        }
        length as u32 / oggstream.ident_hdr.audio_sample_rate
    }
    fn next_sample(&mut self) -> Sample {
        let mut reader = self.reader.lock().unwrap();
        if self.buf.len() <= 10 {
            if let Some(v) = reader.read_dec_packet_itl().unwrap() {
                for i in v {
                    self.buf.push_back(Sample::from(i));
                }
            }
        }
        if let Some(v) = self.buf.pop_front() {
            if self.pos == (reader.ident_hdr.audio_channels - 1) {
                self.pos = 0;
            } else {
                self.pos += 1;
            }
            Sample::from(v)
        } else {
            Sample::end()
        }
    }
    fn sample_rate(&self) -> u32 {
        let reader = self.reader.lock().unwrap();
        reader.ident_hdr.audio_sample_rate
    }
    fn channels (&self) -> u8 {
        let reader = self.reader.lock().unwrap();
        reader.ident_hdr.audio_channels
    }
    fn goto (&self, pos: u64) {
        let mut reader = self.reader.lock().unwrap();
        reader.seek_absgp_pg(pos).unwrap()
    }
}
