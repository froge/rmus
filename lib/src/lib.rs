use std::{
    sync::Mutex,
    sync::Arc,
};
use cpal::traits::*;
use crossbeam::channel;
pub mod decoders;
pub mod traits;
pub use traits::Source;
pub use decoders::{Decoder, FlacDecoder, Mp3Decoder, VorbisDecoder};

#[derive(Debug, Clone, Copy)]
enum SampleFormat {
    I16(i16),
    U16(u16),
    F32(f32),
    None,
}

/// Type abstraction to get a sample as one type, save it and get it as any other
/// possible sample type.
#[derive(Debug, Clone, Copy)]
pub struct Sample(SampleFormat);
impl From<i16> for Sample {
    fn from (val: i16) -> Self {
        Sample(SampleFormat::I16(val))
    }
}
impl From<u16> for Sample {
    fn from (val: u16) -> Self {
        Sample(SampleFormat::U16(val))
    }
}
impl From<f32> for Sample {
    fn from (val: f32) -> Self {
        Sample(SampleFormat::F32(val))
    }
}
impl From<i32> for Sample {
    fn from (val: i32) -> Self {
        Sample(SampleFormat::F32((val as f32 / std::i32::MAX as f32) * 200.0))
    }
}
impl Sample {
    /// Convert the inner sample to an i16.
    fn get_i16(&self) -> Result<i16, ()> {
        match self.0 {
            SampleFormat::I16(v) => Ok(v),
            SampleFormat::U16(v) => Ok((v as i32 - std::i16::MAX as i32) as i16),
            SampleFormat::F32(v) => Ok((v * std::i16::MAX as f32) as i16),
            SampleFormat::None => Err(())
        }
    }
    /// Convert the inner sample to an u16.
    fn get_u16(&self) -> Result<u16, ()> {
        match self.0 {
            SampleFormat::I16(v) => Ok((v as i32 + 32768) as u16),
            SampleFormat::U16(v) => Ok(v),
            SampleFormat::F32(v) => Ok((v * std::u16::MAX as f32) as u16),
            SampleFormat::None => Err(()),
        }
    }
    /// Convert the inner sample to an f32.
    fn get_f32(&self) -> Result<f32, ()> {
        match self.0 {
            SampleFormat::I16(v) => Ok(v as f32 / std::i16::MAX as f32),
            SampleFormat::U16(v) => Ok(v as f32 / std::u16::MAX as f32),
            SampleFormat::F32(v) => Ok(v),
            SampleFormat::None => Err(()),
        }
    }
    fn end () -> Self {
        Sample(SampleFormat::None)
    }
}

/// Plays back samples from a type that implements Source.
///
/// # Example
/// ```
/// use rmus::{Source, VorbisDecoder, Sink};
/// fn main () {
///     let sink = Sink::new(VorbisDecoder::new("example.ogg").unwrap());
/// }
/// ```
pub struct Sink {
    total_length: u32,
    control: Mutex<channel::Sender<(u8, f32)>>,
    end: Mutex<channel::Receiver<bool>>,
    back_recv: Mutex<channel::Receiver<u8>>,
    //thread_channels: (Box<channel::Receiver<(u8, f32)>>, Box<channel::Sender<bool>>, Box<channel::Sender<u8>>),
}

impl Sink {
    /// This creates a new "Sink" and starts playing back the samples its source creates
    pub fn new<T: 'static + Source + Send + Sync> (mut source: T) -> Arc<Self> {
        let (end_send, end_recv) = channel::unbounded();
        let (back_send, back_recv) = channel::unbounded();
        let (control_send, control_recv) = channel::unbounded();
        let sound = Self {
            total_length: source.total_length(),
            control: Mutex::new(control_send),
            end: Mutex::new(end_recv),
            back_recv: Mutex::new(back_recv),
            //thread_channels: (Box::new(control_recv), Box::new(end_send), Box::new(back_send)),
        };
        std::thread::spawn(move || {
        let host = cpal::default_host();
        let device = host.default_output_device().unwrap();
        let format = cpal::Format {
            channels: source.channels() as u16,
            sample_rate: cpal::SampleRate(source.sample_rate()),
            data_type: device.default_output_format().unwrap().data_type,
        };
        let event_loop = Arc::new(host.event_loop());
        event_loop.build_output_stream(&device, &format).unwrap();
        let mut vol = 100;
        let mut paused = true;
        let mut position_samples: u32 = 0;
        let mut position_secs: u32 = 0;
        event_loop.run(move |stream_id, stream_result| {
            match control_recv.try_recv() {
                Ok(v) => match v {
                    (0, _) => {
                        if paused {
                            paused = false;
                        } else {
                            paused = true;
                        }
                    }
                    (1, v) => {
                        let sample_rate = source.sample_rate();
                        let cur_pos: i64 = (position_samples + (sample_rate * position_secs)).into();
                        let new_pos = cur_pos + (sample_rate as f32 * v) as i64;
                        dbg!(cur_pos, new_pos);
                        if new_pos <= 0 {
                            source.goto(0);
                            position_samples = 0;
                            position_secs = 0;
                        } else {
                            source.goto(new_pos as u64);
                            position_secs += (v / sample_rate as f32) as u32;
                            position_samples += (v % sample_rate as f32) as u32;
                        }
                    }
                    (2, v) => {
                        vol += v as u32;
                    },
                    (3, v) => {
                        vol = v as u32;
                    },
                    (4, _) => {
                        back_send.send(vol as u8).unwrap();
                    },
                    (5, _) => {
                        if paused {
                            back_send.send(101).unwrap();
                        } else {
                            back_send.send(102).unwrap();
                        }
                    },
                    (6, _) => {
                        return;
                    },
                    _ => {}
                },
                _ => {}
            }
            let stream_data = match stream_result {
                Ok(data) => data,
                Err(err) => {
                    eprintln!("error on stream {:?}: {}", stream_id, err);
                    return;
                }
            };
            match stream_data {
                cpal::StreamData::Output {
                    buffer: cpal::UnknownTypeOutputBuffer::F32(mut buffer),
                } => {
                    for sample in buffer.chunks_mut(format.channels as usize) {
                        for channel in (0..format.channels as usize).rev() {
                            if position_samples as u32 == format.sample_rate.0 {
                                position_secs += 1;
                                position_samples = 0;
                            } else {
                                position_samples += 1;
                            }
                            sample[channel] = if paused == true {
                                0.0
                            } else {
                                match source.next_sample().get_f32() {
                                    Ok(v) => (v as f32 * vol as f32 / 100.0),
                                    Err(_) => {
                                        if position_secs <= 5 {
                                            0.0
                                        } else {
                                            end_send.send(true).unwrap();
                                            return;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                cpal::StreamData::Output {
                    buffer: cpal::UnknownTypeOutputBuffer::I16(mut buffer),
                } => {
                    for sample in buffer.chunks_mut(format.channels as usize) {
                        for channel in (0..format.channels as usize).rev() {
                            if position_samples as u32 == format.sample_rate.0 {
                                position_secs += 1;
                                position_samples = 0;
                            } else {
                                position_samples += 1;
                            }
                            sample[channel] = if paused == true {
                                0
                            } else {
                                match source.next_sample().get_i16() {
                                    Ok(v) => (v as f32 * vol as f32 / 100.0) as i16,
                                    Err(_) => {
                                        if position_secs <= 5 {
                                            0
                                        } else {
                                            end_send.send(true).unwrap();
                                            return;
                                        }
                                    }
                                }
                            }
                        };
                    }
                }
                cpal::StreamData::Output {
                    buffer: cpal::UnknownTypeOutputBuffer::U16(mut buffer),
                } => {
                    for sample in buffer.chunks_mut(format.channels as usize) {
                        for channel in (0..format.channels as usize).rev() {
                            if position_samples as u32 == format.sample_rate.0 {
                                position_secs += 1;
                                position_samples = 0;
                            } else {
                                position_samples += 1;
                            }
                            sample[channel] = if paused == true {
                                0
                            } else {
                                match source.next_sample().get_u16() {
                                    Ok(v) => (v as f32 * vol as f32 / 100.0) as u16,
                                    Err(_) => {
                                        if position_secs <= 5 {
                                            0
                                        } else {
                                            end_send.send(true).unwrap();
                                            return;
                                        }
                                    }
                                }
                            }
                        };
                    }
                }
                _ => {}
            }
        });
        });
        return Arc::new(sound);
    }
    pub fn toggle_playback(&self) {
        self.control.lock().unwrap()
            .send((0, 1.0))
            .unwrap();
    }
    /// Skips a certain time span in seconds. Can be negative to skip backwards.
    pub fn skip(&self, pos: f32) {
        self.control.lock().unwrap()
            .send((1, pos))
            .unwrap();
    }
    /// Gets the total length in tenths of seconds.
    pub fn get_length(&self) -> u32 {
        self.total_length
    }
    /// Sets the volume relative to the current value.
    pub fn set_rel_vol(&self, vol: i8) {
        self.control.lock().unwrap().send((2, vol as f32)).unwrap();
    }
    pub fn set_vol (&self, vol: u8) {
        self.control.lock().unwrap().send((3, vol as f32)).unwrap();
    }
    pub fn is_finished(&self) -> bool {
        match self.end.lock().unwrap().try_recv() {
            Ok(v) => v,
            Err(_) => false,
        }
    }
    pub fn sleep_until_end(&self) {
        while self.end.lock().unwrap().recv().unwrap() == false {};
    }
    /// Gets the volume from the event loop of the Sink.
    pub fn get_vol(&self) -> u8 {
        self.control.lock().unwrap().send((4, 0.0)).unwrap();
        self.back_recv.lock().unwrap().recv().unwrap()
    }
    /// Checks whether the Sink's playback is paused.
    pub fn paused (&self) -> bool {
        self.control.lock().unwrap().send((5, 0.0)).unwrap();
        if self.back_recv.lock().unwrap().recv().unwrap() == 101 {
            true
        } else {
            false
        }
    }
    pub fn destroy(&self) {
        self.control.lock().unwrap().send((6, 0.0)).unwrap();
    }
}
