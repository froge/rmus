use crate::Sample;
/// A trait that is used as a source of samples
pub trait Source {
    /// Gets the total length of the stream in seconds.
    fn total_length (&self) -> u32;
    /// Gets the next sample in the buffer. This has to be interleaved and, of course, buffered, so
    /// the channels are in backwards order. This is because interleaved is probably faster than
    /// iterating through Vec<VecDeque<Sample>> and having the samples buffered ensures that no
    /// underrun occurs.
    fn next_sample (&mut self) -> Sample;
    /// Gets the sample rate.
    fn sample_rate (&self) -> u32;
    /// Gets the number of channels in the source.
    fn channels (&self) -> u8;
    /// Jumps to a certain location in the file.
    /// The position is to be handled in number of samples.
    fn goto (&self, pos: u64);
}
