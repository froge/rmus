# rmus

A library for high-quality sound playback. _Media player included._

## Example

```rust
use rmus::{Source, Decoder, Sink};
fn main () {
    let sink = Sink::new(Decoder::new("example.ogg").unwrap());
}
```
