use crossbeam::channel;
use crossterm::{
    cursor,
    cursor::MoveTo,
    event::{
        read, DisableMouseCapture, EnableMouseCapture, Event, KeyCode
    },
    queue,
    style::Print,
    terminal::{enable_raw_mode, size, Clear, ClearType},
};
use rmus::*;
use std::{
    env,
    io::{stdin, stdout, Write},
    path::Path,
    process,
    thread,
    time::Duration,
};

#[cfg(not(feature = "fat_arrow"))]
const PLAY_SYMBOL: &'static str = ">";
#[cfg(not(feature = "fat_arrow"))]
const PAUSE_SYMBOL: &'static str = "||";

#[cfg(feature = "fat_arrow")]
const PLAY_SYMBOL: &'static str = "__\n\r\\ \\\n\r \\ \\\n\r / /\n\r/_/";
#[cfg(feature = "fat_arrow")]
const PAUSE_SYMBOL: &'static str = " _    _\n\r| |  | |\n\r| |  | |\n\r| |  | |\n\r|_|  |_|";

fn main() {
    for arg in env::args().skip(1) {
        let source = Decoder::new(&arg).unwrap();
        enable_raw_mode().unwrap();
        let music = Sink::new(source);
        // two name channels because i don't know of a way to do that in a different wae
        let (name_send1, name_recv1) = channel::unbounded();
        let (name_send2, name_recv2) = channel::unbounded();
        let (timer_send, timer_recv) = channel::unbounded();
        // alternate screen to stop it from spamming stdout
        // because clear does not really clear in termion
        let length = music.get_length();
        let length1 = format!("{:0>2}:{:0>2}", length / 60, length % 60);
        let musicc = music.clone();
        // handle keypresses
        thread::spawn(move || {
            let mut name = String::new();
            let mut paused = false;
            let mut vol = 100;
            let up = Event::Key(KeyCode::Up.into());
            let down = Event::Key(KeyCode::Down.into());
            let right = Event::Key(KeyCode::Right.into());
            let left = Event::Key(KeyCode::Left.into());
            let space = Event::Key(KeyCode::Char(' ').into());
            let esc = Event::Key(KeyCode::Esc.into());
            loop {
                let term_size = size().unwrap();
                let event = read().unwrap();
                // manually unwrap name_recv1 for it not to panic because the buffer was empty :D
                match name_recv1.try_recv() {
                    Ok(t) => {
                        name = t;
                    }
                    Err(_) => {}
                }
                /*Event::Mouse(MouseEvent::Up(MouseButton::Left, _, _, _)) => {
                                    if paused {
                                        music.toggle_playback();
                                        paused = false;
                                        // Clear the screen, print the ">" symbol and print the name of the
                                        // song again.
                                        queue!(stdout(), MoveTo(0, 0), Clear(ClearType::FromCursorDown)).unwrap();
                                        disp_send
                                            .send(format!(
                                                "{}{}{}{}{}",
                                                termion::cursor::Goto(0, 0),
                                                termion::clear::AfterCursor,
                                                PLAY_SYMBOL,
                                                termion::cursor::Goto(

                154 |                     Event::Key(KeyCode::Esc.into(termion::terminal_size().unwrap().0 - name.len() as u16
                                                        + 1 as u16,
                                                    1
                                                ),
                                                name
                                            ))
                                            .unwrap();
                                        timer_send.send(0).unwrap();
                                    } else {
                                        music.toggle_playback();
                                        paused = true;
                                        disp_send
                                            .send(format!("{}{}", termion::cursor::Goto(0, 0), PAUSE_SYMBOL))
                                            .unwrap();
                                        timer_send.send(1).unwrap();
                                    }
                                }
                                */
                if event == left {
                    music.skip(-5.0);
                    timer_send.send(2).unwrap();
                }

                if event == right {
                    music.skip(5.0);
                    timer_send.send(3).unwrap();
                }
                if event == up {
                    if vol >= 100 {
                    } else {
                        vol += 5;
                        music.set_vol(vol);
                    }
                }
                if event == down {
                    if vol <= 0 {
                    } else {
                        vol -= 5;
                        music.set_vol(vol);
                    }
                }
                if event == space {
                    if paused {
                        music.toggle_playback();
                        paused = false;
                        // clear the screen, print the ">" symbol and print the name of the
                        // song again
                        queue!(
                            stdout(),
                            MoveTo(0, 0),
                            Clear(ClearType::CurrentLine),
                            Print(PLAY_SYMBOL),
                            MoveTo(term_size.0 - name.len() as u16,
                                0),
                            Print(&name),
                        ).unwrap();
                        stdout().flush().unwrap();
                        timer_send.send(0).unwrap();
                    } else {
                        music.toggle_playback();
                        paused = true;
                        queue!(
                            stdout(),
                            MoveTo(0, 0),
                            Print(PAUSE_SYMBOL),
                        ).unwrap();
                        stdout().flush().unwrap();
                        timer_send.send(1).unwrap();
                    }
                }
                // quit the program on "Esc" keypress
                if event == esc {
                    process::exit(0);
                }
            }
        });

        let length = length1.clone();
        // reprint the screen on size change
        thread::spawn(move || {
            let mut size_old = size().unwrap();
            let name = name_recv2.try_recv().unwrap_or(String::from("lol"));
            loop {
                let size_new = size().unwrap();
                if size_old != size_new {
                    size_old = size_new;
                    let term_size = size().unwrap();
                    queue!(
                        stdout(),
                        cursor::Hide,
                        Clear(ClearType::All),
                        MoveTo(0, 0),
                        Print(PLAY_SYMBOL),
                        MoveTo((term_size.0) - name.len() as u16, 0),
                        Print(&name),
                        MoveTo(term_size.0 - 5, term_size.1),
                        Print(&length),
                    )
                        .unwrap();
                    stdout().flush().unwrap();
                    /*
                       disp_send
                       .send(format!(
                       "{}{}||{}{}",
                       Clear,
                       termion::cursor::Goto(0, 0),
                       termion::cursor::Goto(
                       termion::terminal_size().unwrap().0 - name.len() as u16
                       + 1 as u16,
                       1
                       ),
                       name
                       ))
                       .unwrap();
                       */
                }
                thread::sleep(Duration::from_millis(100));
            }
        });

        let length = length1.clone();
        // just get the filename
        let name = Path::new(&arg)
            .file_name()
            .unwrap()
            .to_str()
            .unwrap()
            .to_owned();
        name_send1.send(name.clone()).unwrap();
        name_send2.send(name.clone()).unwrap();
        let term_size = size().unwrap();
        queue!(
            stdout(),
            cursor::Hide,
            Clear(ClearType::All),
            MoveTo(0, 0),
            Print(PLAY_SYMBOL),
            MoveTo((term_size.0) - name.len() as u16, 0),
            Print(&name),
            MoveTo(term_size.0 - 5, term_size.1),
            Print(&length),
        )
        .unwrap();
        stdout().flush().unwrap();
        // timer
        thread::spawn(move || {
            let mut time_secs = 0;
            let mut time_tenths = 0;
            let mut stop = false;
            loop {
                let term_size = size().unwrap();
                let time = format!("{:0>2}:{:0>2}", time_secs / 60, time_secs % 60);
                // manually unwrap timer_recv for the same reason as above
                match timer_recv.try_recv() {
                    Ok(v) => match v {
                        0 => stop = false,
                        1 => stop = true,
                        2 => {
                            if time_secs < 5 {
                                time_secs = 0;
                            } else {
                                time_secs -= 5;
                            }
                        }
                        3 => {
                            time_secs += 5;
                        }
                        _ => {}
                    },
                    Err(_) => {}
                }
                if !stop {
                    // print the time
                    queue!(stdout(), MoveTo(0, term_size.1), Print(time),).unwrap();
                    stdout().flush().unwrap();
                    time_tenths += 1;
                    if time_tenths == 10 {
                        time_secs += 1;
                        time_tenths = 0;
                    }
                }
                thread::sleep(Duration::from_millis(100));
                //sink.append(source);
                //sink.sleep_until_end();
            }
        });
        musicc.toggle_playback();
        musicc.sleep_until_end();
    }
    process::exit(0);
}
